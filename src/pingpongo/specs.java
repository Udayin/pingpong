package assignment;

public class specs {
	
	// for encoding the message to be send across the network 
	// like if player if 1, x pos = 200 , y pos = 300 then message "1.200.300"
	public  String encode (int a, int b, int c){
		return (Integer.toString(a)+"."+Integer.toString(b)+"."+Integer.toString(c));
	}
	
	String player = "";
	// Following four functions are for decoding the message
	public void PlayerDetails(String a)  // a is like 3.128.100 where P3 has (128,100) location
	{	
		for (int b=0;b<a.length();b++)
		{
			player=player.concat(Character.toString(a.charAt(b)));
		}
		player=player+" ";
		
	}
	  
	// to get the player no. from the encoded message
	public int PlayerNumber()
	{  int c=player.indexOf('.', 0);
		return Integer.parseInt(player.substring(0,c));
	}
	// to get the x coordinate of the player
	public int Xcoordinate()
	{int c=player.indexOf('.', 0);
	int d=player.indexOf('.',c+1);
	return Integer.parseInt(player.substring(c+1,d));
	}
	// to get the y coordinate of the player
	public int Ycoordinate()
	{   int c=player.indexOf('.', 0);
		int d=player.indexOf('.',c+1);
		return Integer.parseInt(player.substring(d+1,player.length()-1));
	}
	

}
