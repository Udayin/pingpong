package pingpongo;

import java.awt.*;
import java.awt.event.*;
class Testing extends Frame
{
  public Testing()
  {
    setSize(300,300);
    setLocation(400,300);
    BackgroundPanel bp = new BackgroundPanel();
    bp.add(new Button("Button"));//to see something on top
    add(bp);
    addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent e){
        System.exit(0);}});
  }
  public static void main(String[] args) {new Testing().setVisible(true);}
}
class BackgroundPanel extends Panel
{
  Image img;
  public BackgroundPanel()
  {
    try
    {
      img = Toolkit.getDefaultToolkit().createImage(new java.net.URL(getClass().getResource("ball.jpg"), "ball.jpg"));
    }
    catch(Exception e){/*handled in paint()*/}
  }
  public void paint(Graphics g)
  {
    super.paint(g);
    if(img != null) g.drawImage(img, 0,0,this.getWidth(),this.getHeight(),this);
    else g.drawString("No Image",100,100);
  }
}
