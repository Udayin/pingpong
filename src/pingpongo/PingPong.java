package pingpongo;


//import java.lang.Math;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
//import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
//import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
//import javax.swing.JScrollPane;
//import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Timer;
import java.awt.Polygon;
//import java.lang.Math;
@SuppressWarnings("serial")
public class PingPong extends JPanel implements KeyListener,ActionListener
{
	private int P ; // for the Position of the paddle i.e, is it either horizontal or vertical , 0=> horizontal,1=> vertical
	public PingPong(int p)
	{timer = new Timer(10, this);
timer.start();

try
{
  img = Toolkit.getDefaultToolkit().createImage(new java.net.URL
		   (getClass().getResource("image3.jpg"), "image3.jpg"));
}
catch(Exception e){}

this.P=p;}
	
	
//Coordinates of the ball
	int x = 80;
int y = 220;

//Speed of the ball
int c=2;
int d=2;

//setters of speed
int t=2;
int u=2;

//Coordinates of player's paddle
int posx=225; //225
int posy=560;

//Coordinates of left paddle
int pos2x=0;
int pos2y=225;   //225

//Coordinates of top paddle
int pos3x=225;
int pos3y=0;

//Coordinates of right paddle
int pos4x=560; 
int pos4y=225;

// TO GET THE LIVES LEFT
int lifeP1=3;
int lifeP2=3;
int lifeP3=3;
int lifeP4=3;

int pause=1;
int start=1;
  





// To send the Player details over network
public int getxP1() { return posx;}
public int getxP2() { return pos2x;}
public int getxP3() { return pos3x;}
public int getxP4() { return pos4x;}

public int getyP1() { return posy;}
public int getyP2() { return pos2y;}
public int getyP3() { return pos3y;}
public int getyP4() { return pos4y;}
//To recieve player details over network and set them
public void setxP1(int a){posx=a;} 
public void setxP2(int a){pos2x=a;} 
public void setxP3(int a){pos3x=a;} 
public void setxP4(int a){pos4x=a;} 

public void setyP1(int a){posy=a;} 
public void setyP2(int a){pos2y=a;} 
public void setyP3(int a){pos3y=a;} 
public void setyP4(int a){pos4y=a;} 






Image img;

private double angle = 0;
Timer timer;

@Override
public void paint(Graphics g) {
    super.paint(g);
    
    
    
    if(img != null) g.drawImage(img, 0,0,this.getWidth(),this.getHeight(),this);
    else g.drawString("",100,100);

    
    
     //left vertical
    Graphics2D leftVert = (Graphics2D) g;
    leftVert.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    leftVert.setColor(Color.darkGray);
    leftVert.fillRect(0, 0, 40, 600); 
    
    //up horizontal
    Graphics2D upHori = (Graphics2D) g;
    upHori.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    upHori.fillRect(40, 0, 520, 40);
    //right vertical
    Graphics2D rightVert = (Graphics2D) g;
    rightVert.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    rightVert.fillRect(560, 0, 40, 600);
    //down hori
    Graphics2D downHori = (Graphics2D) g;
    downHori.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
   
    downHori.fillRect(40, 560, 520, 40);  
    downHori.setColor(Color.BLUE);
    //moving ball
    Graphics2D g2d = (Graphics2D) g;
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
    g2d.fillOval(x, y, 30, 30);
    
    if (lifeP1==3)
    { g2d.setColor(Color.green);}
    else if (lifeP1==2)
    { g2d.setColor(Color.orange);}
    else if (lifeP1==1)
    { g2d.setColor(Color.red);}
   // Player Paddle
    Graphics2D paddle = (Graphics2D) g;
    paddle.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
    RenderingHints.VALUE_ANTIALIAS_ON);
    paddle.fillRect(posx, posy,150, 40);
    
    
    JTextField typingArea = new JTextField(0);
    typingArea.addKeyListener(this);
    add(typingArea, BorderLayout.EAST); 
    //Computer paddles 
    //PADDLE ON LEFT
    if (lifeP2==3)
    { paddle.setColor(Color.green);}
    else if (lifeP2==2)
    { paddle.setColor(Color.orange);}
    else if (lifeP2==1)
    { paddle.setColor(Color.red);}
    
    
    Graphics2D paddle2 = (Graphics2D) g;
    paddle2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
    paddle2.fillRect(pos2x, pos2y,40, 150);
    //PADDLE ON TOP
    if (lifeP3==3)
    { paddle2.setColor(Color.green);}
    else if (lifeP3==2)
    { paddle2.setColor(Color.orange);}
    else if (lifeP3==1)
    { paddle2.setColor(Color.red);}
    
    Graphics2D paddle3 = (Graphics2D) g;
    paddle3.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
    paddle3.fillRect(pos3x, pos3y,150, 40);
  //PADDLE ON RIGHT
    if (lifeP4==3)
    { paddle3.setColor(Color.green);}
    else if (lifeP4==2)
    { paddle3.setColor(Color.orange);}
    else if (lifeP4==1)
    { paddle3.setColor(Color.red);}
    
    Graphics2D paddle4 = (Graphics2D) g;
    paddle4.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
    paddle4.fillRect(pos4x, pos4y,40, 150);
    
    //TEXT FIELDS...
    
    g.setColor(Color.ORANGE);
    g.setFont(new java.awt.Font("Courier New", Font.BOLD, 30));
    
    Graphics2D Lives = (Graphics2D) g;
    Lives.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    Lives.drawString("LIVES LEFT", 650, 50);
    
    //START 
    g.setFont(new java.awt.Font("Courier New", Font.BOLD, 60));
    Graphics2D Start = (Graphics2D) g;
    Start.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    if (start==1 && pause==1) {Start.drawString("Start Game", 130,400);}
    
    //RESUME
    Graphics2D Resume = (Graphics2D) g;
    Resume.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    if (start==0 && pause==1) {Start.drawString("Resumed", 180,400);}
    
    
    g.setFont(new java.awt.Font("Courier New", Font.BOLD, 30));
    g.setColor(Color.CYAN);
    Graphics2D player1Life = (Graphics2D) g;
    player1Life.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    player1Life.drawString("Player1: "+lifeP1, 650, 100);
    
    Graphics2D player2Life = (Graphics2D) g;
    player2Life.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    player2Life.drawString("Player2: "+lifeP2, 650, 150);
    
    Graphics2D player3Life = (Graphics2D) g;
    player3Life.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    player3Life.drawString("Player3: "+lifeP3, 650, 200);
    
    Graphics2D player4Life = (Graphics2D) g;
    player4Life.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    player4Life.drawString("Player4: "+lifeP4, 650, 250);
    
    
    g.setColor(Color.ORANGE);
    g.setFont(new java.awt.Font("Courier New", Font.BOLD, 30));
    
    Graphics2D Colour = (Graphics2D) g;
    Colour.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    Colour.drawString("COLOR:LIFE", 650, 320);
   
    g.setColor(Color.RED);
    Graphics2D Red = (Graphics2D) g;
    Red.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    Red.drawString("Red : 1", 650, 370);
    
    g.setColor(Color.ORANGE);
    Graphics2D Yellow = (Graphics2D) g;
    Yellow.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    Yellow.drawString("Yellow : 2 ", 650, 420);
    
    g.setColor(Color.GREEN);
    Graphics2D Green = (Graphics2D) g;
    Green.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
     Green.drawString("Green : 3 ", 650, 470);
   
      
     
     paddle.setColor(Color.WHITE);
     //int xPoly[] = {150, 250, 325, 375, 450, 275, 100};
     //int yPoly[] = {150, 100, 125, 225, 250, 375, 300};
     
     int xPoly[] = {300, 275,300, 325};
     int yPoly[] = {275, 325,308, 325};

     Polygon poly = new Polygon(xPoly, yPoly, xPoly.length);
     g.fillPolygon(poly);
     g.drawPolygon(poly);
     
     
     int xPoly1[] = {300, 275,300, 325};
     int yPoly1[] = {330, 287, 300,287};

     Polygon poly1 = new Polygon(xPoly1, yPoly1, xPoly1.length);
     
     g.drawPolygon(poly1);
     g.fillPolygon(poly1);
     
    
    
    paddle.setColor(Color.WHITE);
   //Rectangle inside the ball to determine the direction
    
    Rectangle.Float r = new Rectangle.Float(0, 0, 9, 9);
   
    Graphics2D rotor = (Graphics2D) g;
    rotor.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    rotor.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
   // rotor.fillRect(x, y, 12, 12);
    rotor.translate(x+15, y+15);
   rotor.rotate(angle);
   
   rotor.fill(r);
   
   
    
   
   
}



public void moveBall() {
  
	if (pause==0)
	{
	//CORNER CASE
	if ((x==40 && y==40) || (x==530 && y==40) || (x==40 && y==530) || (x==530 && y==530) )
	{ c=-c; d=-d; x=x+c; y=y+d; } else
	
	//UPPER SURFACE
	 if (y<=40 && y>37 && x>40 && x<530 && x-pos3x<=135  && x-pos3x>=-15) 
	{
		if (c>0 && d<0) 
		{
			//y is up (-) and x is right (+)   // up and right
			if (x-pos3x <=50) 
			{if ( x-pos3x<=15) {t=-3;c=-3;} else if ( x-pos3x>15) {t=-2;c=-2;}  //change the ball trajectory 
				 c=-c; d=-d; t=-t; u=-u; x=x+c; y=y+d; } 
			else if (x-pos3x<=70 && x-pos3x>50)
			{  c=0; d=-d;u=-u; x=x+c; y=y+d;}
			else { if ( x-pos3x<=95) {t=2;c=2;} else if ( x-pos3x>95) {t=3;c=3;}
				   d=-d; u=-u; x=x+c; y=y+d;}
		}  else if (c<0 && d<0)
		{
			// y is up(-) and x is left(-)
			if (x-pos3x <=50) 
			{  if ( x-pos3x<=15) {t=-3;c=-3;} else if ( x-pos3x>15) {t=-2;c=-2;}
				d=-d; u=-u; x=x+c; y=y+d;} 
			else if (x-pos3x<=70 && x-pos3x>50)
			{ c=0;  d=-d;u=-u; x=x+c; y=y+d;}
			else 
			{   if (x-pos3x<=95) {t=2;c=2;} else if ( x-pos3x>95) {t=3;c=3;}
				t=-t; c=-c; d=-d;u=-u; x=x+c; y=y+d;}
		}
			else if (c==0 && d<0)
			{
				if (x-pos3x <=60) //50 tha
				{ if (t>0){t=-t; c=t;}else{c=t;} u=-u; d=-d; x=x+c; y=y+d;} //hits the left part of the paddle
				//else if (x-pos3x<=65 && x-pos3x>55)
				//{ u=-u; d=-d; x=x+c; y=y+d;}
				else 
				{if (t<0){t=-t; c=t;}else{c=t;} d=-d; u=-u; x=x+c; y=y+d;}
			}     
	/*	 if (c>0 && d>0) 
 		{
 			//y is up (-) and x is right (+) 
 			d= -d;
 			x = x + c;
 	         y = y+d;	
 		}  else 
 		{
 			// y is up(-) and x is left(-)
 			d=-d;
 			x = x + c;
 	         y = y+d;
 		}   */
	}
	 else if (y<=40 && y>37 && x>40 && x<530)
	 {   d=-d;
	 x = x + c;
     y = y+d;
		 lifeP3 -=1;
	 }
	//DOWN SURFACE
	else if (y>=530 && y<533 && x>40 && x<530 &&  (x-posx <=135) &&  (x-posx >=-15)) //changed from 0 to 30
	{
		
		
		if (c>0 && d>0) 
		{   
			if (x-posx <=50) 
			{   if ( x-posx<=15) {t=-3;c=-3;} else if ( x-posx>15) {t=-2;c=-2;}
				t=-t; u=-u; c=-c; d=-d; x=x+c; y=y+d;} 
			else if (x-posx<=70 && x-posx>50)
			{ 	c=0; u=-u; d=-d; x=x+c; y=y+d;}
			else 
			{   if ( x-posx<=95) {t=2;c=2;} else if ( x-posx>95) {t=3; c=3;}
				u=-u; d=-d; x=x+c; y=y+d;} 		
		}  else if (c<0 && d>0)
		{
			// y is down(+) and x is left(-)
			if (x-posx <=50) 
			{ if ( x-pos3x<=15) {t=-3;c=-3;} else if (x-posx>15) {t=-2;c=-2;}
				u=-u; d=-d; x=x+c; y=y+d;} 
			else if (x-posx<=70 && x-posx>50)
			{ c=0; u=-u; d=-d; x=x+c; y=y+d;}
			else 
			{   if ( x-posx<=95) {t=2;c=2;} else if ( x-posx>95) {t=3;c=3;}
				t=-t; u=-u; c=-c;d=-d; x=x+c; y=y+d;} 	
		}
		else if (c==0 && d>0) 
		{
			if (x-posx <=60) 
			{ if (t<0){c=t;} else {t=-t; c=t;} d=-d; x=x+c; y=y+d;} //hits the left part of the paddle
		//	else if (x-posx<=65 && x-posx>55)
		//	{  u=-u;d=-d; x=x+c; y=y+d;}
			else 
			{if (t>0) {c=t;} else {t=-t; c=t;} d=-d; x=x+c; y=y+d;} 	
		} 
		
	/*	if (c>0 && d>0) 
		{
			//y is down (+) and x is right (+)
			d= -d;
			x = x + c;
	         y = y+d;
			
		}  else 
		{
			// y is down(+) and x is left(-)
			d=-d;
			x = x + c;
	         y = y+d;
		}   */
	} 
	else if (y>=530 && y<533 && x>40 && x<530)
	{
		 d=-d;
    	 x = x + c;
         y = y+d;
    		 lifeP1 -=1;
	}
	//LEFT SURFACE
	else if (x<=40 && x>37 && y>40 && y<530 && (y-pos2y<=135) && (y-pos2y>=-15) )
	{
		if (c<0 && d>0)  //left and down /
		{   
			if (y-pos2y <=50) 
			{   if (y-pos2y<=15 ){d=-3; u=-3;} else if (y-pos2y>15 ){d=-2; u=-2;} 
				t=-t; u=-u; c=-c; d=-d; x=x+c; y=y+d;} 
			else if (y-pos2y<=70 && y-pos2y>=50)
			{ d=0; t=-t; c=-c; x=x+c; y=y+d;}
			else 
			{   if (y-pos2y<=95 ){d=2; u=2;} else if (y-pos2y>95 ){d=3; u=3;} 
				t=-t; c=-c; x=x+c; y=y+d;} 		
		}  else if (c<0 && d<0)   //left and up
		{
			
			if (y-pos2y <=50) 
			{    if (y-pos2y<=15 ){d=-3; u=-3;} else if (y-pos2y>15 ){d=-2; u=-2;} 
				t=-t; c=-c; x=x+c; y=y+d;} //hits the left part of the paddle
			else if (y-pos2y<=70 && y-pos2y>=50)
			{ d=0; c=-c; t=-t; x=x+c; y=y+d;}
			else 
			{   if (y-pos2y<=95 ){d=2; u=2;} else if (y-pos2y>95 ){d=3; u=3;}
				t=-t; u=-u; c=-c;d=-d; x=x+c; y=y+d;} 	
		}
		else if (c<0 && d==0) 
		{
	if (y-pos2y <=60) 
	{ if (u>0){u=-u;d=u;} else { d=u;} c=-c; t=-t; x=x+c; y=y+d;} //hits the left part of the paddle
   // else if (y-pos2y<=65 && y-pos2y>=55)
   // { t=-t; c=-c; x=x+c; y=y+d;}
	else
	{if (u>0) {d=u;} else {u=-u; d=u;} t=-t; c=-c; x=x+c; y=y+d;} 	
		}  
	/*	if (c>0 && d>0) 
		{
			//y is down (+) and x is right (+)
			c= -c;
			x = x + c;
	         y = y+d;
			
		}  else 
		{
			// y is down(+) and x is left(-)
			 c=-c;
			 x = x + c;
	         y = y+d;
		}   */
	}  
	else if (x<=40 && x>37 && y>40 && y<530)
	{
		c=-c;
   	 x = x + c;
        y = y+d;
   		 lifeP2 -=1;
	}
	//RIGHT SURFACE
	else if (x>=530 && x<533 && y>40 && y<530 && (y-pos4y<=135) && (y-pos4y>=-15))
	{
		if (c>0 && d>0)  //right and down 
		{   
			if (y-pos4y <=50) 
			{   if (y-pos4y<=15 ){d=-2; u=-2;} else if (y-pos4y>15 ){d=-1; u=-1;} 
				t=-t; u=-u; c=-c; d=-d; x=x+c; y=y+d;} 
			else if (y-pos4y<=70 && y-pos4y>=50)
			{ d=0; t=-t; c=-c; x=x+c; y=y+d;}
			else 
			{   if (y-pos4y<=95 ){d=1; u=1;} else if (y-pos4y>95 ){d=2; u=2;}
				t=-t; c=-c; x=x+c; y=y+d;}
		//	else 
		//	{ t=-t; c=-c; x=x+c; y=y+d;}
		}  else if (c>0 && d<0) // right and up
		{
			
			if (y-pos4y <=50) 
			{   if (y-pos4y<=15 ){d=-2; u=-2;} else if (y-pos4y>15 ){d=-1; u=-1;} 
				t=-t; c=-c; x=x+c; y=y+d;} //hits the left part of the paddle
			else if (y-pos4y<=70 && y-pos4y>=50)
			{ d=0; c=-c; t=-t; x=x+c; y=y+d;}
			else
			{    if (y-pos4y<=95 ){d=1; u=1;} else if (y-pos4y>95 ){d=2; u=2;}
				t=-t; u=-u; c=-c;d=-d; x=x+c; y=y+d;} 
			
		}
		else if (c>0 && d==0) 
		{
	if (y-pos4y <=60) 
	{ if (u>0){u=-u;d=u;} else { d=u;} c=-c; t=-t; x=x+c; y=y+d;} //hits the left part of the paddle
  //  else if (y-pos4y<=65 && y-pos4y>=55)
  //  { t=-t; c=-c; x=x+c; y=y+d;}
	else 
	{if (u>0) {d=u;} else {u=-u; d=u;} t=-t; c=-c; x=x+c; y=y+d;} 	
		}  
	/*	if (c>0 && d>0) 
		{
			//y is down (+) and x is right (+)
			c= -c;
			x = x + c;
	         y = y+d;
			
		}  else 
		{
			// y is down(+) and x is left(-)
			 c=-c;
			 x = x + c;
	         y = y+d;
		}  */
	}
	else if (x>=530 && x<533 && y>40 && y<530)
	{
		c=-c;
      	 x = x + c;
           y = y+d;
      		 lifeP4 -=1;
	}

	//to manage high speed situations of the ball
/*  	else if (c>2)
	{  //left and right paddles.
		if ((x<40)  && (y-pos2y <=150) &&  (y-pos2y >=0))
		{	 c= -c;
			 x = x + c;
	         y = y+d;
		}
		else if ((x>530)  && (y-pos4y <=150) &&  (y-pos4y >=0))
		{	 c= -c;
			 x = x + c;
	         y = y+d;
		}
		//up and down paddles
		else if ((y<40) && (x-pos3x<=150) && (x-pos3x>=0))
		{
			d= -d;
			x = x + c;
	         y = y+d;
		}
		else if ((y>530) && (x-posx<=150) && (x-posx>=0))
		{   d= -d;
			x = x + c;
	        y = y+d;	}
		else 
		{    x = x + c;
	         y = y+d;}
			
	}
	*/
	
	
	else
		//NORMAL CASE FOR MOVEMENT OF THE BALL
        //if (x>=40 && y>=40 && x<=530 && y<=530)
 	{x = x + c;
     y = y+d;  
     
     //CPU Player for now
     CPUp4 p1=new CPUp4(c,d,posx,posy,x,y); //for P2 
     
     CPUp1 p2=new CPUp1(c,d,pos2x,pos2y,x,y); //for P2
              
     CPUp2 p3=new CPUp2(c,d,pos3x,pos3y,x,y); //for P3
     
     
     CPUp3 p4=new CPUp3(c,d,pos4x,pos4y,x,y); //for P4
    
     
     
     switch (P) {
     	case 1 : 
     		pos2y=p2.setMove();
            pos3x=p3.setMove(); 
            pos4y=p4.setMove();
            break;
     	case 2:
     		posx=p1.setMove();
            pos3x=p3.setMove(); 
            pos4y=p4.setMove();
            break;
     	case 3:
     		pos2y=p2.setMove();
            posx=p1.setMove(); 
            pos4y=p4.setMove();
            break;
     	case 4:
     		pos2y=p2.setMove();
            pos3x=p3.setMove(); 
            posx=p1.setMove();
            break;
     }
     
 	}
	}
	
	
}

 


 


/** Handle the key typed event from the text field. */
public void keyTyped(KeyEvent e) {
    displayInfo(e, "KEY TYPED: ");
}

/** Handle the key pressed event from the text field. */
public void keyPressed(KeyEvent e) {
    displayInfo(e, "KEY PRESSED: ");
}

/** Handle the key released event from the text field. */
public void keyReleased(KeyEvent e) {
    displayInfo(e, "KEY RELEASED: ");
}

protected void displayInfo(KeyEvent e, String s){
          
    int id = e.getID();
    if (id == KeyEvent.KEY_TYPED) {
    	int keyCode = e.getKeyCode();
    	switch( keyCode ) { 
       // case KeyEvent.VK_UP:
    	case 38:
        	
        	if (pos2y>=70&(P==2))  
			{pos2y = pos2y-30;}
        	else if (pos4y>70&&(P==4))
        	{pos4y = pos4y -30;}
            break;
        //case KeyEvent.VK_DOWN:
    	case 40:
        	if (pos2y<=380&&(P==2))
			{pos2y=pos2y+30;}	 
        	else if (pos4y<=380&&(P==4))
        	{pos4y=pos4y+30;}
            break;
        //case KeyEvent.VK_LEFT:
    	case 37:
        	if (posx>=60&&(P==1))
      		{posx = posx -30;}
        	else if (pos3x>=60&&(P==3))
      		{pos3x = pos3x -30;}
        		
            break;
        //case KeyEvent.VK_RIGHT :
    	case 39:
        	 if (posx<=380&&(P==1))
      		{posx = posx +30;}
        	 else  if (pos3x<=380&&(P==3))
       		{pos3x = pos3x +30;}
            break;
    	case KeyEvent.VK_SPACE :
    		if (pause==0) {pause=1;} else {pause=0;}
    		break;
     }
    	
    	
    	
    	}
     else { 
    	 int keyCode = e.getKeyCode();
     	switch( keyCode ) { 
        // case KeyEvent.VK_UP:
     	case 38:
         	
         	if (pos2y>=70&&(P==2))  
 			{pos2y = pos2y-30;
 			
 			}
         	else if (pos4y>=70&&(P==4))
         	{pos4y = pos4y -30;}
             break;
         //case KeyEvent.VK_DOWN:
     	case 40:
         	if (pos2y<=380&&(P==2))
 			{pos2y=pos2y+30;}	 
         	else if (pos4y<=380&&(P==4))
         	{pos4y=pos4y+30;}
             break;
         //case KeyEvent.VK_LEFT:
     	case 37:
         	if (posx>=60&&(P==1))
       		{posx = posx -30;}
         	else if (pos3x>=60&&(P==3))
       		{pos3x = pos3x -30;}
         		
             break;
         //case KeyEvent.VK_RIGHT :
     	case 39:
         	 if (posx<=380&&(P==1))
       		{posx = posx +30;}
         	 else  if (pos3x<=380&&(P==3))
        		{pos3x = pos3x +30;}
             break;
     	case KeyEvent.VK_SPACE :
    		if (pause==0) {pause=1;} 
    		break;    
     	case KeyEvent.VK_ENTER: 
     		if (pause==1) {pause=0;}
     		start=0;
     		break;
             
      }
    	
    }
   
}

















public static void main(String[] args) throws InterruptedException {
	
	JFrame frame = new JFrame("Sample Frame");
	
    PingPong game = new PingPong(1);
    frame.add(game);
    frame.setSize(900, 700);
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    
    while (true) {
        game.moveBall();
        game.repaint();
        Thread.sleep(5);
        
    }    
}     
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		 angle += 0.02; 
		 
		    repaint();
		
	}
}