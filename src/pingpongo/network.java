 //import java.awt.event.ActionEvent;
import java.io.*;
import java.net.*;
import java.awt.BorderLayout;
import java.awt.Color;

//package net.codejava.swing.jcheckbox;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
//import javax.swing.JRadioButton;
import javax.swing.*;


import javax.swing.JFrame;

class InputThread extends Thread {
      
      private MulticastSocket msocket;
      private DatagramPacket dataR;
      private PingPong game;
      private int P; // player number of that pc
      
      specs dataP = new specs(); 
    
      public InputThread (MulticastSocket msock ,InetAddress group_no , int Port_no, PingPong g, int p)
    {     this.game=g;
          this.P=p;
              msocket= msock;
              start();               
      }
      
      public void run()
      {     
              
              
              byte[] b = new byte[1000];
              String dataTemp;
              try{  
                  for(;;)
                      {
                          dataR = new DatagramPacket(b, b.length);
                          msocket.receive(dataR);
                          dataTemp = new String(dataR.getData(),0,dataR.getLength());
                          System.out.println(dataTemp);
                          
                          dataP.PlayerDetails(dataTemp);
                          int i = dataP.PlayerNumber();
                          
                          int x = dataP.Xcoordinate();
                          int y = dataP.Ycoordinate();
                          dataP.player = "";
                          switch (i) {
                            case 1 :
                              if (P!=1){
                              game.setxP1(x);
                              game.setyP1(y);}
                              break;
                            case 2 :
                              if (P!=2){
                              game.setxP2(x);
                              game.setyP2(y);}
                              break;
                            case 3 :
                              if (P!=3){
                              game.setxP3(x);
                              game.setyP3(y);
                              }
                              break;
                            case 4 :
                              if(P!=4){
                              game.setxP4(x);
                              game.setyP4(y);}
                              break;
                            } 
                          
                          
                         
                              
                          
                         
                      }

              } catch (IOException e) {
                      System.out.println("Exit...");
                      e.printStackTrace();
              } finally {
                      msocket.close();
              }
      }
      
}

public class network {
  
  //static int p = 2;
  static boolean p1=true ;
  static boolean p2=true ;
  static boolean p3=true;
  static boolean p4=true;
  private static int level , no ;  
  //private static int slptime= 4;
  static String Instring;
  static int port = 1234;
  static String ipaddress = "228.2.3.4" ;
  static int x,y;
  static PingPong game = new PingPong();
  static specs dataP= new specs();
      
      
      public static void main(String[] args) throws IOException
      {   
      
      InetAddress group = InetAddress.getByName(ipaddress);
      
              if (!group.isMulticastAddress()) {
                       System.out.println( "The address: " + group + " is not multicast address");
                       System.exit(0);

              }
              
              AbsolutePositionDemo apd = new AbsolutePositionDemo(game);
              SwingUtilities.invokeLater(new Runnable() {
              public void run() {
                apd.setVisible(true);
              }
              });
              
             
              
              try {
                       
                      MulticastSocket s = new MulticastSocket(port);
                       
                      s.setReuseAddress(true);
                      
                      
                      s.setLoopbackMode(false);
                      
                      
                      s.setTimeToLive(2);
                     
                      s.joinGroup(group);
                      
                     
                      int posx=25;
                      int posy=25;
                     
                      new InputThread(s,group,port,game,game.P);
                        
                      for(;;)
                      { 
                        
                        
                        switch (game.P) {
                        case 1 :
                          if (posx!=game.getxP1())
                          {
                            Instring = dataP.encode(1, game.getxP1(), game.getyP1()) ;
                            
                                posx=game.getxP1();
                              posy=game.getyP1();
                            
                          }else { Instring = "";} 
                          break;
                        case 2 :
                           
                          
                          if (posy!=game.getyP2())
                          { 
                            Instring = dataP.encode(2, game.getxP2(), game.getyP2()) ;
                            
                                posx=game.getxP2();
                              posy=game.getyP2();
                            
                          }else { Instring = "";}
                          break;
                        case 3 :
                          if (posx!=game.getxP3())
                          {
                            Instring = dataP.encode(3, game.getxP3(), game.getyP3()) ;
                            
                                posx=game.getxP3();
                              posy=game.getyP3();
                            
                          }else { Instring = "";}
                          break;
                        case 4 :
                          if (posy!=game.getyP4())
                          {
                            Instring = dataP.encode(4, game.getxP4(), game.getyP4()) ;
                            
                                posx=game.getxP4();
                              posy=game.getyP4();
                            
                          }else { Instring = "";}
                          break;
                        } 
                        if (Instring==null) break;
                        if (Instring!=""){
                          
                        DatagramPacket dp = new DatagramPacket(Instring.getBytes(), Instring.length(),group, port);
                        s.send(dp);     
                        } 
                        
                        game.moveBall();
                        game.repaint();
                        try {Thread.sleep(game.slptime);}catch(Exception e){};
                       
                        
                        
                      }

                      //System.out.println("Leaving the Group...");
                      s.leaveGroup(group);
                      s.close();
                      
                      
                           



                      
              } catch (Exception err){
                        System.err.println("ERR: Can not join the group " + err);
                        err.printStackTrace();
                        System.exit(1);
              }
            
            
            
    
}
     
}
