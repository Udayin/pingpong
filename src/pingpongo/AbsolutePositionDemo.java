package pingpongo;



import java.awt.BorderLayout;
import java.awt.Color;

//package net.codejava.swing.jcheckbox;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
//import javax.swing.JRadioButton;
import javax.swing.*;



public class AbsolutePositionDemo extends JFrame {
    public AbsolutePositionDemo() {
        initializeUI();
    }
    
    
    //Setting levels--easy,medium,hard
    public int level=1;
    //Setting no. of players-- single player and multiplayer
    public int no=1;
    //Setting which player position the player takes
    public int player=1;
    
    private JCheckBox checkboxOne = new JCheckBox("Easy");
	private JCheckBox checkboxTwo = new JCheckBox("Medium");
	private JCheckBox checkboxThree = new JCheckBox("Hard");
	private JButton select = new JButton("READY!");
	
	private JCheckBox checkboxFour=new JCheckBox("Single Player");
	private JCheckBox checkboxFive=new JCheckBox("Multi Player");
	
	private JCheckBox checkboxSix=new JCheckBox("Player1");
	private JCheckBox checkboxSeven=new JCheckBox("Player2");
	private JCheckBox checkboxEight=new JCheckBox("Player3");
	private JCheckBox checkboxNine=new JCheckBox("Player4");

	
	public boolean MoveToPP=false;
	
    private void initializeUI() {
    	
        setSize(700, 700);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel(null);
        
        

        

       
        checkboxOne.setBounds(50, 350, 150,50); 
        checkboxTwo.setBounds(50,400,150,50);
        checkboxThree.setBounds(50, 450, 200, 50);
        
        select.setBounds(250, 520, 200, 50);
        
        checkboxFour.setBounds(230, 350, 200, 50);
        checkboxFive.setBounds(230, 400, 200, 50);
        
        checkboxSix.setBounds(480, 350, 300, 50);
        checkboxSeven.setBounds(480, 388, 300, 50);
        checkboxEight.setBounds(480, 425, 300, 50);
        checkboxNine.setBounds(480, 460, 300, 50);
        
        

       //ImageIcon icon = new ImageIcon("images/ball.jpg"); 
       //JLabel label = new JLabel(icon); 
       JLabel label = new JLabel(new ImageIcon(getClass().getResource("ball.jpg")));
       label.setBounds(250,50, 230, 230);
        panel.add(label); 
        
        JLabel label1 = new JLabel("PING PONG");
        label1.setBounds(190,270,400,70);
        panel.add(label1);
        label1.setFont(new java.awt.Font("Courier New", Font.BOLD, 60));
		label1.setForeground(Color.orange);
        
       panel.add(checkboxOne);
		panel.add(checkboxTwo);
		panel.add(checkboxThree);
		panel.add(select);
		panel.add(checkboxFour);
		panel.add(checkboxFive);
		panel.add(checkboxSix);
		panel.add(checkboxSeven);
		panel.add(checkboxEight);
		panel.add(checkboxNine); 
		
		
		//add(radio);
		
		checkboxOne.setFont(new java.awt.Font("Arial", Font.BOLD, 25));
		checkboxOne.setForeground(Color.ORANGE);
		
		checkboxTwo.setFont(new java.awt.Font("Arial", Font.BOLD, 25));
		checkboxTwo.setForeground(Color.orange);
		checkboxThree.setFont(new java.awt.Font("Arial", Font.BOLD, 25));
		checkboxThree.setForeground(Color.orange);
		
		checkboxFour.setFont(new java.awt.Font("Arial", Font.BOLD, 25));
		checkboxFour.setForeground(Color.yellow);
		checkboxFive.setFont(new java.awt.Font("Arial", Font.BOLD, 25));
		checkboxFive.setForeground(Color.yellow);
		
		checkboxSix.setFont(new java.awt.Font("Arial", Font.BOLD, 25));
		checkboxSix.setForeground(Color.orange);
		checkboxSeven.setFont(new java.awt.Font("Arial", Font.BOLD, 25));
		checkboxSeven.setForeground(Color.orange);
		checkboxEight.setFont(new java.awt.Font("Arial", Font.BOLD, 25));
		checkboxEight.setForeground(Color.orange);
		checkboxNine.setFont(new java.awt.Font("Arial", Font.BOLD, 25));
		checkboxNine.setForeground(Color.orange);
		
		select.setFont(new java.awt.Font("Arial", Font.BOLD, 30));
		select.setForeground(Color.darkGray);
		select.setOpaque(true);
		select.setBackground(Color.orange);
		
		//checkboxThree.setLocation(300, 500);
		//add(labelSum);
		
		//textFieldSum.setEditable(false);
		//add(textFieldSum);
		
		// add action listener for the check boxes
		ActionListener actionListener = new ActionHandler(); 
		checkboxOne.addActionListener(actionListener);
		checkboxTwo.addActionListener(actionListener);
		checkboxThree.addActionListener(actionListener);
		checkboxFour.addActionListener(actionListener);
		checkboxFive.addActionListener(actionListener);
		checkboxSix.addActionListener(actionListener);
		checkboxSeven.addActionListener(actionListener);
		checkboxEight.addActionListener(actionListener);
		checkboxNine.addActionListener(actionListener);
		select.setActionCommand("OK");
	    select.addActionListener(new ButtonClickListener()); 

	    
	    
        setContentPane(panel);
       // getContentPane().setBackground(new Color(40, 100, 150));
        getContentPane().setBackground(Color.BLACK);
    }
    
    
    class ActionHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			JCheckBox checkbox = (JCheckBox) event.getSource();
			if (checkbox.isSelected()) {
				if (checkbox == checkboxOne) {
					level=1;  //easy mode
					checkboxTwo.setSelected(false);
					checkboxThree.setSelected(false);
				} else if (checkbox == checkboxTwo) {
					level=2;   //medium mode
					checkboxOne.setSelected(false);
					checkboxThree.setSelected(false);
				} else if (checkbox == checkboxThree) {
					level=3;   //hard mode
					checkboxOne.setSelected(false);
					checkboxTwo.setSelected(false);
				}
				else if (checkbox == checkboxFour) {
		            no=1;      //single player
					checkboxFive.setSelected(false);
				}
				else if (checkbox == checkboxFive) {
					no=2;    //multiplayer
					checkboxFour.setSelected(false);
					
				}
               else if (checkbox == checkboxSix) {
					player=1; //down paddle position
					checkboxSeven.setSelected(false);
					checkboxEight.setSelected(false);
					checkboxNine.setSelected(false);
					
				}
            else if (checkbox == checkboxSeven) {
	            player=2;   //left paddle position
            	checkboxSix.setSelected(false);
				checkboxEight.setSelected(false);
				checkboxNine.setSelected(false);
	
                 }
               else if (checkbox == checkboxEight) {
	               player=3;   //up paddle position
            	   checkboxSix.setSelected(false);
					checkboxSeven.setSelected(false);
					checkboxNine.setSelected(false);
	
                 }
              else if (checkbox == checkboxNine) {
	              player=4;   //right paddle position
            	  checkboxSix.setSelected(false);
					checkboxSeven.setSelected(false);
					checkboxEight.setSelected(false);
					
	
                  }
			} else {
				if (checkbox == checkboxOne) {} 
				else if (checkbox == checkboxTwo) {}
				else if (checkbox == checkboxThree) {}
			       } 
			
			//JButton check=(JButton)event.getSource();
			
		
			
		}
	}
	
	private class ButtonClickListener implements ActionListener{
	      public void actionPerformed(ActionEvent e) {
	         String command = e.getActionCommand();  
	         if( command.equals( "OK" ))  {
	            
	        	 
	        	
	      }		
	   }
	}
	

    public static void main(String[] args) throws InterruptedException{
    	AbsolutePositionDemo hello=new AbsolutePositionDemo();
        hello.setVisible(true);
   /*   while (true){
    	  
         
          if (hello.MoveToPP)
          {
          	System.out.println("YO");
          }
      }   */
            
        
       
    }    
    	
    	
}